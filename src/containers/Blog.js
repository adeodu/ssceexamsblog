
import React from 'react'
import { getRouteProps, Link } from 'react-static'
//

export default getRouteProps(({ posts }) => (
  <div>
    <h1>It is blog time.</h1>
    <br />
    All Posts:
    <ul>
      {posts.map(post => (
        <li key={post.id}>
          <Link to={`/ssceexams-blog-v1/blog/post/${post.id}/index.html`}>{post.title}</Link>
        </li>
      ))}
    </ul>
  </div>
))
