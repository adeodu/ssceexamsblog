import React from 'react'
import { getSiteProps } from 'react-static'
import styled from 'styled-components'
//
import logoImg from '../desktp800.jpg'

export default getSiteProps(() => (
  <div>
    <h1>SSCEexams Blog - Latest Blogs</h1>
    <div>
      <h3>Front-end web development</h3>
      <p>
        Front-end web development is the practice of producing HTML, CSS and JavaScript for a website
        or Web Application so that a user can see and interact with them directly. The challenge
        associated with front-end development is that the tools and techniques used to create the front
        end of a website change constantly and so the developer needs to constantly be aware of how the
        field is developing. The objective of designing a site is to ensure that when the users open up
        the site they see the information in a format that is easy to read and relevant. This is further
        complicated by the fact that users now use a large variety of devices with varying screen sizes
        and resolutions thus forcing the designer to take into consideration these aspects when designing
        the site. They need to ensure that their site comes up correctly in different browsers
        (cross-browser), different operating systems (cross-platform) and different devices
        (cross-device), which requires careful planning on the side of the developer.
      </p>
      <img src={logoImg} alt="" />
      <p>
        There are several tools available that can be used to develop the front end of a website,
        and understanding which tools are best fit for specific tasks marks the difference between
        developing a hacked site and a well designed, scalable site.</p>
      <h3>HyperText Markup Language (HTML)</h3>
      <p>
        HyperText Markup Language (HTML) is the backbone of any website development process,
        without which a web page does not exist. Hypertext means that text has links, termed
        hyperlinks, embedded in it. When a user clicks on a word or a phrase that has a hyperlink,
        it will bring another webpage. A markup language indicates text can be turned into images,
        tables, links, and other representations. It is the HTML code that provides an overall
        framework of how the site will look. HTML was developed by Tim Berners-Lee. The latest
        version of HTML is called HTML5 and was published on October 28, 2014 by the W3 recommendation.
        This version contains new and efficient ways of handling elements such as video and audio files.
        It is very useful in creating web pages.
      </p>
      <h3>Cascading Style Sheets (CSS)</h3>
      <p>
        Cascading Style Sheets (CSS) controls the presentation aspect of the site and allows your site
        to have its own unique look. It does this by maintaining style sheets which sit on top of other
        style rules and are triggered based on other inputs, such as device screen size and resolution.
      </p>
    </div>
  </div>
))
