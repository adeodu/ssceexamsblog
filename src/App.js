import React from 'react'
import { Router, Link } from 'react-static'
import styled, { injectGlobal } from 'styled-components'
//
import Routes from 'react-static-routes'
import logoImg from 'logo.png'

injectGlobal`
  body {
    font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial,
      'Lucida Grande', sans-serif;
    font-weight: 300;
    font-size: 16px;
    margin: 0;
    padding: 0;
  }
`

const AppStyles = styled.div`
  a {
    text-decoration: none;
    color: #108db8;
    font-weight: bold;
  }

  nav {
    width: 100%;
    background: #9ED9CD;

    a {
      color: #333;
      padding: 0.5rem;
      display: inline-block;
      font-size: 20px;

      img {
        height: 1.8rem;
        border-radius: 10px;
      }
    }
  }

  .content {
    padding: 1rem;
  }

  img {
    max-width: 100%;
  }
`

export default () => (
  <Router>
    <AppStyles>
      <nav>
        <Link to="/ssceexams-blog-v1/index.html">
          <img src={logoImg} alt="" />
        </Link>
        <Link to="/ssceexams-blog-v1/index.html">Home</Link>
        <Link to="/ssceexams-blog-v1/about/index.html">About</Link>
        <Link to="/ssceexams-blog-v1/blog/index.html">Blog</Link>
      </nav>
      <div className="content">
        <Routes />
      </div>
    </AppStyles>
  </Router>
)
